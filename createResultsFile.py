import json

from EmbeddedIndex import EmbeddedIndex
from createSets import embeddings


def calculate_result(concept, obj, num=4):
    hansard_result = hansard.search_phrase(concept, num)
    snomed_result = snomed.search_phrase(concept, num)
    foodon_result = foodon.search_phrase(concept, num)
    hansard_found = []
    hansard_not_found = []
    snomed_found = []
    snomed_not_found = []
    foodon_found = []
    foodon_not_found = []
    hansard_label_found = []
    snomed_label_found = []
    foodon_label_found = []

    if 'hansard' in obj:
        for result in hansard_result:
            for tag in obj['hansard']:
                if result[1] == tag:
                    if tag not in hansard_found and tag not in hansard_not_found:
                        hansard_found.append(tag)
                        hansard_label_found.append("[" + result[0] + "] " + tag)
                        hansard_result.remove(result)
                else:
                    if tag not in hansard_found and tag not in hansard_not_found:
                        hansard_not_found.append(tag)
    if 'snomedct' in obj:
        for result in snomed_result:
            for tag in obj['snomedct']:
                tagStr = tag.split('/')
                tstr = tagStr[len(tagStr) - 1]
                if result[1] == tstr:
                    if tag not in snomed_found and tag not in snomed_not_found:
                        snomed_found.append(tag)
                        snomed_label_found.append("[" + result[0] + "] " + tag)
                        snomed_result.remove(result)
                else:
                    if tag not in snomed_found and tag not in snomed_not_found:
                        snomed_not_found.append(tag)
    if 'foodon' in obj:
        for result in foodon_result:
            for tag in obj['foodon']:
                tagStr = tag.split('/')
                tstr = tagStr[len(tagStr) - 1]
                if result[1] == tstr:
                    if tag not in foodon_found and tag not in foodon_not_found:
                        foodon_found.append(tag)
                        foodon_label_found.append("[" + result[0] + "] " + tag)
                        foodon_result.remove(result)
                else:
                    if tag not in foodon_found and tag not in foodon_not_found:
                        foodon_not_found.append(tag)
    data = {}
    data['hansard_found'] = hansard_label_found
    data['hansard_not_found'] = [f"[{hansardMap.get(t)}] {hUri}{t}" for t in hansard_not_found]
    data['hansard_our_results'] = transform_result_snomedct(hansard_result, 'hansard')
    data['snomed_found'] = snomed_label_found
    data['snomed_not_found'] = [f"[{snomedMap.get(t)}] {sUri}{t}" for t in snomed_not_found]
    data['snomed_our_results'] = transform_result_snomedct(snomed_result, 'snomedct')
    data['foodon_found'] = foodon_label_found
    data['foodon_not_found'] = [f"[{foodOnMap.get(t)}] {fUri}{t}" for t in foodon_not_found]
    data['foodon_our_results'] = transform_result_snomedct(foodon_result, 'foodon')
    return concept, data


def transform_result_snomedct(results, type):
    list = []
    for r in results:
        if type == 'snomedct':
            res = f"[{r[0]}] {sUri}{r[1]}"
        elif type == 'foodon':
            res = f"[{r[0]}] {fUri}{r[1]}"
        else:
            res = f"[{r[0]}] {hUri}{r[1]}"
        list.append(res)
    return list

    # the size of the Bert word embedding


d = 3072

hansardMap = {}
foodOnMap = {}
snomedMap = {}

hUri = 'https://www.english-corpora.org/hansard/s/semtags.asp?l=3&c1='
fUri = 'http://purl.obolibrary.org/obo/'
sUri = 'http://purl.bioontology.org/ontology/SNOMEDCT/'

with open('data/parsed/hansard-all.json') as f:
    hansardMap = json.load(f)

with open('data/parsed/foodon-all.json') as f:
    foodOnMap = json.load(f)

with open('data/parsed/snomedct-all.json') as f:
    snomedMap = json.load(f)

# init embedding
bertEmbeddings = embeddings['glove-mean']()
bertEmbeddingsDimension = bertEmbeddings.embedding_length

hansard = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
hansard.load('data/indexes/hansard-glove-mean')

foodon = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
foodon.load('data/indexes/foodon-glove-mean')

snomed = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
snomed.load('data/indexes/snomedct-glove-mean')

list = [1, 5, 10, 15, 30]
for i in list:
    with open('data/food-onto-map/food-ontomap.json') as f:
        json_file = json.load(f)
        root = {}
        for concept in json_file:
            print(json_file[concept])
            word, data = calculate_result(concept, json_file[concept], i)
            root[word] = data
        filename = 'data/results-glove-' + str(i) + '.json'
        with open(filename, 'w') as outfile:
            json.dump(root, outfile, indent=2)
