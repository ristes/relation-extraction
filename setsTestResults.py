import os
import pickle

from flair.data import Sentence


def evaluate(index, allName, model, dataset):
    name = f'{dataset}-{model}'
    fn = f'data/emb/{name}.pickle'
    obj = None
    if os.path.exists(fn):
        with open(fn, 'rb') as f:
            obj = pickle.load(f)
    concept_embeddings = None
    concept_tags = None
    if obj:
        concept_embeddings = obj['embeddings']
        concept_tags = obj['tags']
    else:
        concept_embeddings, concept_tags = get_embeddings(index, allName)

    with open(fn, 'wb') as f:
        pickle.dump({
            'embeddings': concept_embeddings,
            'tags': concept_tags
        }, f)

    arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30]
    # arr = [3, 5, 8, 10, 15, 30]
    for i in arr:
        true_positive, false_positive, false_negative, total_tags, num_lines = calculate_results(index,
                                                                                                 concept_embeddings,
                                                                                                 concept_tags, i)

        precision = true_positive / (true_positive + false_positive)
        recall = true_positive / (true_positive + false_negative)
        f1 = 2 * precision * recall / (precision + recall)
        accuracy = true_positive / total_tags
        with open('data/results.txt', 'a') as out_file:
            out_file.write(f'{dataset}\t{model}\t{str(i)}\t{str(true_positive)}\t{false_positive}\t{false_negative}'
                           f'\t{str(total_tags)}\t{str(num_lines)}\n')


def get_embeddings(index, set_filename):
    concept_tags = {}
    concept_embeddings = {}
    with open(set_filename, "r") as syn:
        line = syn.readline()
        while line:
            vals = line.split("|")
            concept = vals[1].strip()
            tags = vals[2].strip().split(";")
            if vals[0] != "ID":
                sentence = Sentence(concept)
                index.embedding.embed(sentence)
                # print('Printing for word: ' + phrase)
                token = sentence.embedding.cpu().detach().numpy().reshape(1, index.d)
                concept_embeddings[concept] = token
                concept_tags[concept] = tags
            line = syn.readline()

        return concept_embeddings, concept_tags


def calculate_results(index, concept_embeddings, concept_tags, num=4):
    total_tags_num = 0
    true_positive = 0
    false_positive = 0
    false_negative = 0
    num_lines = 0
    for concept, token in concept_embeddings.items():
        num_lines += 1
        tags = concept_tags[concept]
        total_tags_num += len(tags)
        dist, indices = index.search_index(token, num)

        result = []
        array = indices[0]
        for indices in array:
            result.append(index.wordsUri[indices])
        found_positive = 0
        for r in result:
            for t in tags:
                if str(r).__eq__(t):
                    found_positive += 1

        true_positive += found_positive
        false_negative += len(tags) - found_positive
        false_positive += len(result) - found_positive

    return true_positive, false_positive, false_negative, total_tags_num, num_lines
