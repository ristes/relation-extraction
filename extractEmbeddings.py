import json

import faiss
import numpy as np
import pandas as pd
import torch
import transformers as ppb  # pytorch transformers
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split

model_class, tokenizer_class, pretrained_weights = (ppb.BertModel, ppb.BertTokenizer, 'bert-base-uncased')
tokenizer = tokenizer_class.from_pretrained(pretrained_weights)
model = model_class.from_pretrained(pretrained_weights)

padded = []
tokenized = []

filename='data/parsed/snomedct-all.json'
# def create_index_from_json_file(filename):
with open(filename) as f:
    json_obj = json.load(f)
    for uri in json_obj:
        concept = json_obj[uri]
        tokenized.append(tokenizer.encode(concept, add_special_tokens=True))

max_len = 0
for i in tokenized:
    if len(i) > max_len:
        max_len = len(i)

padded = np.array([i + [0] * (max_len - len(i)) for i in tokenized])
print(np.array(padded).shape)


attention_mask = np.where(padded != 0, 1, 0)
print(attention_mask.shape)

input_ids = torch.tensor(np.array(padded))
with torch.no_grad():
    last_hidden_states = model(input_ids)
    features = last_hidden_states[0][:, 0, :].numpy()
    normalized=[]
    for g in features:
        x=g.reshape(1,768)
        faiss.normalize_L2(x)
        normalized.append(x)
