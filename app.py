# pip install flask
# pip install -U flask-cors

import json

import numpy as np
from flair.embeddings import BertEmbeddings
from flair.embeddings import DocumentPoolEmbeddings
from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin

from EmbeddedIndex import EmbeddedIndex
from createSets import embeddings, datasets

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


app.json_encoder = NumpyEncoder
# the size of the Bert word embedding
d = 3072

# init embedding
bertEmbeddings = DocumentPoolEmbeddings([BertEmbeddings()])
bertEmbeddingsDimension = 3072

hansard = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
hansard.load('data/indexes/hansard-bert-mean')

foodon = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
foodon.load('data/indexes/foodon-bert-mean')

snomed = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
snomed.load('data/indexes/snomedct-bert-mean')

foodOntoMap = {}
with open('data/food-onto-map/food-ontomap.json') as f:
    foodOntoMap = json.load(f)


@app.route("/<entity>/<param>")
@cross_origin()
def search_concept(entity, param):
    hansard_result = hansard.search_phrase(entity, int(param))
    snomed_result = snomed.search_phrase(entity, int(param))
    foodon_result = foodon.search_phrase(entity, int(param))
    data = {
        'hansard': hansard_result,
        'snomed': snomed_result,
        'foodon': foodon_result,
        'ontoMap': foodOntoMap.get(entity)
    }
    return jsonify(data)


indexes = {}
model_name = 'glove-mean'
embed = embeddings[model_name]()
for dataset in datasets:
    print(f'{dataset}-{model_name}')
    index = EmbeddedIndex(embed, embed.embedding_length)
    index.load(f'data/indexes/{dataset}-{model_name}')
    indexes[dataset] = index


@app.route("/search", methods=['GET', 'POST'])
@cross_origin()
def search():
    phrase = request.values['phrase']
    num_results = int(request.values['num_results'])
    result = {}
    for ds, ix in indexes.items():
        result[ds] = ix.search_phrase(phrase, num_results)

    return jsonify(result)


@app.route("/", methods=['GET', 'POST'])
@cross_origin()
def get_results():
    param = 5
    filename = 'data/results-glove-5.json'
    dropdown_list = [1, 5, 10, 15, 30]
    if request.method == 'POST':
        param = request.form['param']
        filename = 'data/results-glove-' + str(param) + '.json'

    with open(filename) as f:
        json_file = json.load(f)
        return render_template('result_template.html', result=json_file, parameters=dropdown_list, param=param)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
