import faiss
# pip install flair
from flair.data import Sentence
from flair.embeddings import BertEmbeddings

from flair.embeddings import DocumentPoolEmbeddings

from EmbeddedIdIndex import EmbeddedIdIndex
from EmbeddedIndex import EmbeddedIndex
from triples_manipulation import to_sentence

# the size of the Bert word embedding
d = 3072

# init embedding
bertEmbeddings = DocumentPoolEmbeddings([BertEmbeddings()])
bertEmbeddingsDimension = 3072

# myIndex = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
# tableIndex = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
# columnIndex = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)
# tokenIndex = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)

DBpediaIndex = EmbeddedIndex(bertEmbeddings, bertEmbeddingsDimension)

DBpediaIndex.init_index()

DBpediaIndex.load('data/dbPediaIndex')
DBpediaIndex.load_words('data/words.pickle')
DBpediaIndex.load_words_uri('data/words_uri.pickle')


# DBpediaIndex.search_phrase("City")  # List[uris]

# myIndex.init_index()
# tableIndex.init_index()
# columnIndex.init_index()
# tokenIndex.init_index()
#
# columns = set()
# tables = set()
#
# with open("data/dbpedia_2016-10.nt", "r") as syn:
#     line = syn.readline()
#     while line:
#         vals = line.split(' ', 2)
#         uri = vals[0]
#         pred = vals[1]
#         obj = vals[2]
#         predStr = pred.replace("<", "").replace(">", "")
#         predStr = predStr.split("#")
#         if len(predStr) > 1 and predStr[1] == "label":
#             objStr = obj.split("@")
#             lang = objStr[1].split(".")[0].strip()
#             if lang == "en":
#                 DBpediaIndex.embed_resource(uri, objStr[0])
#         # myIndex.embed_phrase(desc)
#         line = syn.readline()
# #
# DBpediaIndex.write('data/dbPediaIndex')
# DBpediaIndex.save_words('data/words.pickle')
# DBpediaIndex.save_words_uri('data/words_uri.pickle')

# myIndex.store('data/target_schema')
#
# for tbl in tables:
#     tableIndex.embed_phrase(tbl)
# tableIndex.store('data/target_tables')
#
# for col in columns:
#     columnIndex.embed_phrase(col)
# columnIndex.store('data/target_columns')
#
# tokens = set()
#
# for tbl in tables:
#     tbl_tokens = tbl.split(" ")
#     for t in tbl_tokens:
#         tokens.add(t)
#
# for col in columns:
#     col_tokens = col.split(" ")
#     for t in col_tokens:
#         tokens.add(t)
#
# for t in tokens:
#     if t:
#         tokenIndex.embed_phrase(t)
#
# tokenIndex.store('data/target_tokens')
#
# myIndex.restore('data/target_schema')

# myIndex.build_index('data/dbpedia_2016-10.nt', to_sentence)
# myIndex.load_words('data/words.pickle')

# myIndex.write('data/dbpediaIndex')

# myIndex.load('data/dbpediaIndex')
# with open("data/matched.csv", "w") as o:
#     with open("data/detect.txt") as f:
#         phrase = f.readline().strip().lower()
#         while phrase:
#             result = myIndex.search_phrase(phrase, 5)
#             o.write(f"{phrase}\n{result}\n")
#             phrase = f.readline().strip().lower()


# with open("data/source.tsv", "r") as syn:
#     line = syn.readline()
#     while line:
#         vals = line.split('\t')
#         tbl = vals[1].replace("_", " ")
#         col = vals[0].replace("_", " ")
#         if tbl.__eq__('LOAN'):
#             desc = tbl + " " + col
#             result = myIndex.search_phrase(desc, 5)
#             print(f"{desc}\n"
#                   f"{result}\n"
#                   f"{tableIndex.search_phrase(tbl, 5)}\n"
#                   f"{columnIndex.search_phrase(col, 5)}\n")
#         line = syn.readline()
#
# s1 = Sentence('LOAN Loan ID')
# s2 = Sentence('LOAN.Loan ID')
# s3 = Sentence('LOAN. Loan ID')
# s4 = Sentence('LOAN  Loan ID')
# bertEmbeddings.embed(s1)
# bertEmbeddings.embed(s2)
# bertEmbeddings.embed(s3)
# bertEmbeddings.embed(s4)
#
# def embed_vector(token):
#     t = token.embedding.cpu().detach().numpy()
#     # add the token's array to the indexing set
#     t = t.reshape((1, t.size))
#     return t
#
#
# t1 = embed_vector(s1)
# t2 = embed_vector(s2)
# t3 = embed_vector(s3)
# t4 = embed_vector(s4)
