# pip install flair
import json
import os

from flair.embeddings import TransformerDocumentEmbeddings, DocumentPoolEmbeddings, CharacterEmbeddings, \
    SentenceTransformerDocumentEmbeddings, DocumentRNNEmbeddings, WordEmbeddings, BertEmbeddings, StackedEmbeddings, \
    FlairEmbeddings, ELMoEmbeddings, TransformerWordEmbeddings

from EmbeddedIndex import EmbeddedIndex
from setsTestResults import evaluate


def create_index_from_json_file(index, filename):
    with open(filename) as f:
        json_obj = json.load(f)
        for uri in json_obj:
            concept = json_obj[uri]
            index.embed_phrase(uri.strip(), concept.strip())


def create_token_index_from_json_file(index, filename):
    with open(filename) as f:
        json_obj = json.load(f)
        for uri in json_obj:
            concept = json_obj[uri]
            index.embed_phrase_tokens(uri.strip(), concept.strip())


embeddings = {
    'elmo-sentence': lambda: ELMoEmbeddings(),
    'bert-nli-sentence': lambda: SentenceTransformerDocumentEmbeddings('bert-base-nli-cls-token'),
    'bert-wk-sentence': lambda: SentenceTransformerDocumentEmbeddings('bert-base-nli-stsb-wkpooling'),
    'bert-sentence': lambda: TransformerDocumentEmbeddings('bert-base-uncased', fine_tune=False),
    'bert-sentence-4l': lambda: TransformerDocumentEmbeddings('bert-base-uncased', fine_tune=False,
                                                              layers="-1,-2,-3,-4"),
    'character-mean': lambda: DocumentPoolEmbeddings([CharacterEmbeddings()]),
    'glove-rnn': lambda: DocumentRNNEmbeddings([WordEmbeddings('glove')]),
    'fastTextNews-rnn': lambda: DocumentRNNEmbeddings([WordEmbeddings('news')]),
    'gpt2-sentence': lambda: TransformerDocumentEmbeddings('gpt2', fine_tune=False),
    'xlnet-sentence': lambda: TransformerDocumentEmbeddings('xlnet-base-cased', fine_tune=False),
    'distilroberta-sentence': lambda: TransformerDocumentEmbeddings('distilroberta-base', fine_tune=False),
    'albert-sentence': lambda: TransformerDocumentEmbeddings('albert-base-v2', fine_tune=False),
    'bert-mean': lambda: DocumentPoolEmbeddings([BertEmbeddings()]),
    'glove-mean': lambda: DocumentPoolEmbeddings([WordEmbeddings('glove')]),
    'flair-mean': lambda: DocumentPoolEmbeddings([
        StackedEmbeddings([
            WordEmbeddings('glove'),
            FlairEmbeddings('news-forward-fast'),
            FlairEmbeddings('news-backward-fast')
        ])
    ])
}
wordEmbeddings = {
    'glove': lambda: WordEmbeddings('glove'),
    'fastTextNews-rnn': lambda: WordEmbeddings('news'),
    'bert': lambda: TransformerWordEmbeddings(),
    'gpt2': lambda: TransformerDocumentEmbeddings('gpt2'),
    'xlnet': lambda: TransformerDocumentEmbeddings('xlnet-base-uncased')
}

datasets = ['foodon', 'hansard', 'snomedct']


def create_word_indexes():
    for model_name, embed in wordEmbeddings.items():
        for dataset in datasets:
            print(f'{dataset}-{model_name}')
            index = EmbeddedIndex(embed, embed.embedding_length)
            if not os.path.exists(f'data/indexes/{dataset}-{model_name}.pickle'):
                create_token_index_from_json_file(index, f"data/parsed/{dataset}-all.json")
                index.store(f'data/indexes/{dataset}-{model_name}')
                print(f"{dataset}-{model_name} finshed")
            else:
                print(f'skipping: {dataset}-{model_name}')


def create_indexes(model_name):
    embed = embeddings[model_name]()
    for dataset in datasets:
        print(f'{dataset}-{model_name}')
        index = EmbeddedIndex(embed, embed.embedding_length)
        if not os.path.exists(f'data/indexes/{dataset}-{model_name}.pickle'):
            create_index_from_json_file(index, f"data/parsed/{dataset}-all.json")
            index.store(f'data/indexes/{dataset}-{model_name}')
            print(f"{dataset}-{model_name} finshed")
        else:
            print(f'skipping: {dataset}-{model_name}')


def evaluate_indexes(model_name):
    embed = embeddings[model_name]()
    for dataset in datasets:
        print(f'{dataset}-{model_name}')
        index = EmbeddedIndex(embed, embed.embedding_length)
        index.load(f'data/indexes/{dataset}-{model_name}')
        evaluate(index, f'data/food-onto-map/{dataset}_set_all.csv', model_name, dataset)

#
# create_indexes()
# evaluate_indexes()
