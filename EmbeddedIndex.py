import pickle

import faiss
import numpy as np
from flair.data import Sentence


class EmbeddedIndex:
    index = None
    words = []
    wordsUri = []

    def __init__(self, embedding, d):
        self.embedding = embedding
        self.d = d
        self.words = []
        self.wordsUri = []
        self.embeddings = []
        quantizer = faiss.IndexFlatIP(self.d)
        self.index = faiss.IndexIVFFlat(quantizer, d, 50, faiss.METRIC_INNER_PRODUCT)

    def load(self, index_path):
        pickle_in = open(index_path + ".pickle", "rb")
        obj = pickle.load(pickle_in)
        self.words = obj.get('words')
        self.wordsUri = obj.get('uris')
        self.embeddings = obj.get('embeddings')
        emb = np.array(self.embeddings).reshape(len(self.embeddings), self.d)
        if not self.index.is_trained:
            self.index.train(emb)
        self.index.add(emb)
        print("Read index from faiss file")

    def store(self, filename):
        pickle_out = open(filename + ".pickle", "wb")
        pickle.dump({
            'words': self.words,
            'uris': self.wordsUri,
            'embeddings': self.embeddings
        }, pickle_out)
        pickle_out.close()
        print("Saved index with faiss")

    def embed_phrase(self, uri, word):
        sentence = Sentence(word)
        self.embedding.embed(sentence)
        t = sentence.embedding.cpu().detach().numpy()
        # add the token's array to the indexing set
        t = t.reshape((1, t.size))
        # print(word)
        self.index_add_word_vector(uri, word, t)

    def embed_phrase_tokens(self, uri, phrase):
        sentence = Sentence(phrase)
        self.embedding.embed(sentence)
        for token in sentence:
            t = token.embedding.cpu().detach().numpy()
            # add the token's array to the indexing set
            t = t.reshape((1, t.size))
            # print(word)
            self.index_add_word_vector(uri, token.text, t)

    def embed_concatenate_tokens(self, uri, phrase, num_tokens=10):
        sentence = Sentence(phrase)
        self.embedding.embed(sentence)
        concat=np.array()
        for token in sentence:
            t = token.embedding.cpu().detach().numpy()
            t = t.reshape((1, t.size))
            # zalepi go t na concat
        # ako brojot na tokeni e pokratok, dopuni so (food embedding)
        self.index_add_word_vector(uri, phrase, concat)

    def index_add_word_vector(self, uri, word, word_vector):
        if word not in self.words:
            faiss.normalize_L2(word_vector)
            # self.index.add(word_vector)
            self.words.append(word)
            self.wordsUri.append(uri)
            self.embeddings.append(word_vector)

    def search_index(self, xq, top_k_results=4):
        """
        :param xq: Query words embeddings
        :param index: The index being searched
        :param top_k_results: How many results are we looking for
        :return:
        """
        faiss.normalize_L2(xq)
        D, I = self.index.search(xq, top_k_results)
        return D, I

    def search_phrase_tokens(self, phrase, num=4):
        sentence = Sentence(phrase)
        self.embedding.embed(sentence)

        result = []
        for token in sentence:
            emb = token.embedding.cpu().detach().numpy().reshape(1, self.d)
            dist, indices = self.search_index(emb, num)
            array = indices[0]
            j = 0
            for indices in array:
                print(self.wordsUri[indices] + " " + self.words[indices])
                result.append((self.words[indices], self.wordsUri[indices], dist[0, j]))
                j += 1

        # sort results and return top {num}

    def search_phrase(self, phrase, num=4):
        sentence = Sentence(phrase)
        self.embedding.embed(sentence)
        # print('Printing for word: ' + phrase)
        token = sentence.embedding.cpu().detach().numpy().reshape(1, self.d)
        dist, indices = self.search_index(token, num)
        array = indices[0]
        j = 0
        result = []
        for indices in array:
            print(self.wordsUri[indices] + " " + self.words[indices])
            # print(self.words[indices])
            # print(dist[0, j])
            result.append((self.words[indices], self.wordsUri[indices], dist[0, j]))
            j += 1
        return result
