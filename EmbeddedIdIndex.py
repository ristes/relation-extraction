import faiss
import numpy as np
import pickle
from flair.data import Sentence

from triples_manipulation import load_triples_from_file


class EmbeddedIdIndex:
    index = None
    data = {}

    def __init__(self, embedding, d):
        self.embedding = embedding
        self.d = d

    def init_index(self):
        self.index = faiss.IndexIDMap(faiss.IndexFlatIP(self.d))
        return self.index

    def embed_resource(self, uri, label):
        sentence = Sentence(uri + " " + label)
        self.embedding.embed(sentence)
        for token in sentence:
            # get the numpy array from the token embedding
            t = token.embedding.cpu().detach().numpy()
            # add the token's array to the indexing set
            t = t.reshape((1, t.size))
            self.index_add_word_vector(token.text, t)

    def index_add_word_vector(self, word, word_vector):
        # array = np.array([[len(self.data)]])
        # array = array.reshape((1, word_vector.size))
        print(word_vector.shape)
        self.index.add_with_ids(word_vector, word_vector)
        self.data[np.array(range(0, len(self.data)))] = word
