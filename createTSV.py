import csv

with open('data/parsed/hansard_parsed.tsv', 'wt') as out_file:
    out_file.write('semtags' + '\t' + 'labels'+'\n')
    with open('data/food-onto-map/hansard_set_all.csv', "r") as syn:
        line = syn.readline()
        while line:
            vals = line.split("|")
            word = vals[1]
            uris = vals[2].strip()
            if vals[0] != "ID":
                out_file.write(str(uris.strip()) + "\t" + word.strip() + "\n")
                print(str(uris.strip()) + "\t" + word.strip() + "\n")
            line = syn.readline()

print('All done !')
