# pip install rdflib
from rdflib import Graph


def load_triples_from_file(location):
    graph = Graph()
    graph.parse(location, format="nt")
    triples = []
    print("Graph size " + str(len(graph)))
    for subject, predicate, obj in graph:
        triples.append([subject, predicate, obj])
    return triples, graph


def to_sentence(graph, triple):
    return graph.label(triple[0]) + " " + graph.label(triple[1]) + " " + graph.label(triple[2])
    # return triple[0] + " " + triple[1] + " " + triple[2]
