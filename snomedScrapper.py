import requests
import json

import rdflib
import json
from rdflib import Literal, RDF, RDFS
from requests import Response

parents = {}
arr = []
doneConcepts = {}
todoConcepts = []
with open('data/parsed/snomed_parsed.tsv', 'r') as f:
    line = f.readline()
    while line:
        vals = line.split("\t")
        concept_id = vals[0]
        if vals[0] != "semtags":
            doneConcepts[concept_id] = concept_id
        line = f.readline()

finished = []
with open('data/snomed.json') as s:
    finished = json.load(s)

for res in finished:
    if res.get('children') is None:
        children = requests.get(
            'https://browser.ihtsdotools.org/snowstorm/snomed-ct/browser/MAIN/2020-07-31/concepts/' + res[
                'conceptId'] + '/children/?form=inferred')
        if children.status_code == 200:
            res['children'] = json.loads(children.text)
        else:
            print(children.text)
        print(res['conceptId'])

for res in finished:
    cId = res['conceptId']
    if not doneConcepts.get(cId):
        todoConcepts.append(cId)
    if res.get('children'):
        children = res['children']
        for c in children:
            cId = c['conceptId']
            if not doneConcepts.get(cId):
                todoConcepts.append(cId)

with open('data/snomed-with-children.json', 'w') as s:
    json.dump(finished, s)


def create_resource(obj, pId):
    if len(obj) != 0:
        for childObj in obj:
            cId = childObj['conceptId']
            parents[cId] = pId
            if doneConcepts.get(cId):
                continue
            r = requests.get(
                'https://browser.ihtsdotools.org/snowstorm/snomed-ct/browser/MAIN/2020-07-31/concepts/' + childObj[
                    'conceptId'] + '/children/?form=inferred')
            json_obj = json.loads(r.text)
            create_resource(json_obj, cId)
            write_to_file('data/parsed/snomed_parsed.tsv', 'data/snomed_json_file.txt', childObj)
            with open('data/snomed_json_file.txt', 'a') as json_out_file:
                json.dump(json_obj, json_out_file)
                json_out_file.write('\n')


def write_to_file(tsv_filename, json_filnename, json_obj):
    with open(tsv_filename, 'a') as out_file:
        out_file.write(json_obj['conceptId'] + "\t" + json_obj['pt']['term'] + "\n")
        print(json_obj['conceptId'] + "\t" + json_obj['pt']['term'] + "\n")
    with open(json_filnename, 'a') as json_out_file:
        json.dump(json_obj, json_out_file)
        json_out_file.write('\n')


r = requests.get(
    'https://browser.ihtsdotools.org/snowstorm/snomed-ct/browser/MAIN/2020-07-31/concepts/762766007/children/?form=inferred')
root_obj = json.loads(r.text)
create_resource(root_obj, '762766007')


foodOn = {}

g = rdflib.Graph()
g.load('data/foodon_ontology.owl')
# g.load('http://purl.obolibrary.org/obo/foodon/releases/2020-09-14/foodon.owl')
g.load('http://purl.obolibrary.org/obo/bfo/2.0/bfo.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/chebi_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/ecocore_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/envo_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/foodon_product_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/gaz_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/geem_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/hancestro_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/langual_deprecated_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/metadata_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/ncbitaxon_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/ncit_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/obi_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/pato_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/peco_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/po_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/product_type_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/ro_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/robot_pasta.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/robot_wine.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/uberon_import.owl')
g.load('http://purl.obolibrary.org/obo/foodon/imports/uo_import.owl')

for s, o in g.subject_objects(rdflib.term.URIRef('http://www.w3.org/2000/01/rdf-schema#label')):

    if not foodOn.get(str(s)):
        key = str(s)
        foodOn[key.replace('http://purl.obolibrary.org/obo/', '')] = str(o)


for k in foodOn:
    print(k)

with open('data/parsed/foodon-all.json', 'w') as f:
    json.dump(foodOn, f)




fideo = rdflib.Graph()
fideo.load('/storage/workspace/food-base-web/data/fideo.owl')

for s,p,o in fideo:
    print(s+" "+p+" "+o)

